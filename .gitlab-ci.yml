include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

default:
  image: "rust:1-bookworm"

variables:
  CARGO_HOME: "$CI_PROJECT_DIR/cargo"
  CARGO_INCREMENTAL: "0"

rustfmt:
  stage: test
  needs: []
  before_script:
    - rustup component add rustfmt
  script:
    - cargo fmt --check -v

rust-tests:
  stage: test
  needs: []
  cache:
    key: $CI_JOB_NAME
    paths:
      - cargo/
      - target/
  before_script:
    - rustc --version
  script:
    - cargo test --locked

cargo-audit:
  stage: test
  needs: []
  cache: []
  rules:
    - changes:
        - Cargo.toml
        - Cargo.lock
  variables:
    CARGO_AUDIT_VERSION: "v0.21.1"
  before_script:
    - curl -LO "https://github.com/rustsec/rustsec/releases/download/cargo-audit%2F${CARGO_AUDIT_VERSION}/cargo-audit-x86_64-unknown-linux-musl-${CARGO_AUDIT_VERSION}.tgz"
    - mkdir -p "${CARGO_HOME}/bin"
    - tar -xf "cargo-audit-x86_64-unknown-linux-musl-${CARGO_AUDIT_VERSION}.tgz" -O "cargo-audit-x86_64-unknown-linux-musl-${CARGO_AUDIT_VERSION}/cargo-audit" > "${CARGO_HOME}/bin/cargo-audit"
    - chmod +x "${CARGO_HOME}/bin/cargo-audit"
  script:
    - cargo audit -f Cargo.lock

build-amd64:
  stage: build
  cache:
    key: cargo-amd64
    paths:
      - cargo/
      - target/
  before_script:
    - rustc --version
  script:
    - cargo build --locked --release
  artifacts:
    paths:
      - target/release/chrony_exporter

build-aarch64:
  stage: build
  cache:
    key: cargo-aarch64
    paths:
      - cargo/
      - target/
  before_script:
    - apt-get --quiet update
    - apt-get --quiet --yes install gcc-aarch64-linux-gnu
    - rustup target add aarch64-unknown-linux-gnu
    - rustc --version
  variables:
    CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER: "aarch64-linux-gnu-gcc"
  script:
    - cargo build --locked --target=aarch64-unknown-linux-gnu --release
  artifacts:
    paths:
      - target/aarch64-unknown-linux-gnu/release/chrony_exporter

rpm-amd64:
  stage: deploy
  needs:
    - build-amd64
  cache:
    key: cargo-amd64
    paths:
      - cargo/
      - target/
  before_script:
    - cargo install cargo-generate-rpm
  script:
    - strip -s target/release/chrony_exporter
    - cargo generate-rpm
  artifacts:
    paths:
      - target/generate-rpm/*.rpm

rpm-aarch64:
  stage: deploy
  needs:
    - build-aarch64
  cache:
    key: cargo-aarch64
    paths:
      - cargo/
      - target/
  before_script:
    - apt-get --quiet update
    - apt-get --quiet --yes install binutils-aarch64-linux-gnu
    - cargo install cargo-generate-rpm
  script:
    - aarch64-linux-gnu-strip -s target/aarch64-unknown-linux-gnu/release/chrony_exporter
    - cargo generate-rpm --target=aarch64-unknown-linux-gnu --auto-req disabled
  artifacts:
    paths:
      - target/aarch64-unknown-linux-gnu/generate-rpm/*.rpm

deb-amd64:
  stage: deploy
  needs:
    - build-amd64
  before_script:
    - cargo install cargo-deb
  script:
    - cargo deb --no-build --separate-debug-symbols
  artifacts:
    paths:
      - target/debian/*.deb

deb-aarch64:
  stage: deploy
  needs:
    - build-aarch64
  before_script:
    - apt-get --quiet update
    - apt-get --quiet --yes install gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu
    - mkdir .cargo
    - echo -e '[target.aarch64-unknown-linux-gnu]\nstrip = { path = "aarch64-linux-gnu-strip" }\nobjcopy = { path = "aarch64-linux-gnu-objcopy" }' > .cargo/config.toml
    - cargo install cargo-deb
  script:
    - cargo deb --target=aarch64-unknown-linux-gnu --no-build --separate-debug-symbols
  artifacts:
    paths:
      - target/aarch64-unknown-linux-gnu/debian/*.deb
