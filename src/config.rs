use clap::{arg, command};
use std::{net, str::FromStr};
use tracing::level_filters::LevelFilter;

/// The logging destination.
#[derive(Debug, Clone, Copy)]
pub(crate) enum LogDestination {
    /// Standard output
    Stdout,
    /// Standard error
    Stderr,
    /// The systemd journal
    Journal,
    /// Automatically choose. It tries to determine if `Journal` is appropriate,
    /// otherwise it falls back to `Stderr`.
    Auto,
}

impl FromStr for LogDestination {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "stdout" => Ok(LogDestination::Stdout),
            "stderr" => Ok(LogDestination::Stderr),
            "journal" => Ok(LogDestination::Journal),
            "auto" => Ok(LogDestination::Auto),
            _ => Err(()),
        }
    }
}

/// Program options.
///
/// These are parsed via the command-line and filled in by `clap`.
#[derive(Debug)]
pub(crate) struct ProgramOptions {
    /// Log destination
    pub(crate) logging_dest: LogDestination,
    /// Minimium log level to filter on
    pub(crate) logging_level: LevelFilter,
    /// Addresses to try listening on
    pub(crate) listen_addr: Vec<net::SocketAddr>,
}

impl ProgramOptions {
    /// Parse the command-line arguments
    pub(crate) fn parse_args() -> Self {
        let long_version = if let Some(v) = option_env!("VERGEN_GIT_DESCRIBE") {
            format!(
                "{} ({} {})",
                v,
                option_env!("VERGEN_GIT_COMMIT_TIMESTAMP").unwrap_or("Unknown"),
                option_env!("VERGEN_GIT_SHA").unwrap_or("Unknown")
            )
        } else {
            env!("CARGO_PKG_VERSION").to_string()
        };
        let args = command!()
            .long_version(long_version)
            .args(&[
                arg!(-v --verbose ... "Sets the level of verbosity"),
                arg!(-o --output <DEST> "Where to write output log messages ('auto' means 'journal' if it looks like we're inside a systemd service, otherwise it's 'stderr')").required(false).value_parser(["auto", "stdout", "stderr", "journal"]).default_value("auto"),
                arg!(export_addr: --listen <ADDR> "TCP address to listen on [default: [::]:9701 or 0.0.0.0:9701]. Ignored if systemd passes in any connections. May be specified multiple times.").required(false).action(clap::ArgAction::Append),
            ])
            .get_matches();

        let level = match args.get_count("verbose") {
            0 => LevelFilter::INFO,
            1 => LevelFilter::DEBUG,
            _ => LevelFilter::TRACE,
        };

        const DEFAULT_PORT: u16 = 9701;

        ProgramOptions {
            logging_level: level,
            listen_addr: args.get_many::<String>("export_addr").map_or(
                vec![
                    net::SocketAddr::new(net::Ipv6Addr::UNSPECIFIED.into(), DEFAULT_PORT),
                    net::SocketAddr::new(net::Ipv4Addr::UNSPECIFIED.into(), DEFAULT_PORT),
                ],
                |addresses| {
                    addresses
                        .map(|address| {
                            net::SocketAddr::from_str(address).unwrap_or_else(|_| {
                                panic!("cannot parse '{address}' as a socket address")
                            })
                        })
                        .collect()
                },
            ),
            logging_dest: LogDestination::from_str(args.get_one::<String>("output").unwrap())
                .unwrap(),
        }
    }

    /// Initialize the logging
    pub(crate) fn init_logging(&self) {
        use tracing_subscriber::{filter::Targets, layer::SubscriberExt, util::SubscriberInitExt};

        let filter = Targets::new().with_target(env!("CARGO_PKG_NAME"), self.logging_level);

        match (
            self.logging_dest,
            libsystemd::logging::connected_to_journal(),
        ) {
            (LogDestination::Stdout, _) => {
                let stdout_layer = tracing_subscriber::fmt::layer();

                tracing_subscriber::Registry::default()
                    .with(stdout_layer)
                    .with(filter)
                    .init();
            }
            (LogDestination::Stderr, _) | (LogDestination::Auto, false) => {
                let stderr_layer = tracing_subscriber::fmt::layer().with_writer(std::io::stderr);

                tracing_subscriber::Registry::default()
                    .with(stderr_layer)
                    .with(filter)
                    .init();
            }
            (LogDestination::Journal, _) | (LogDestination::Auto, true) => {
                let journald_layer =
                    tracing_journald::layer().expect("cannot connect to systemd-journald socket");

                tracing_subscriber::Registry::default()
                    .with(journald_layer)
                    .with(filter)
                    .init();
            }
        };
    }
}
