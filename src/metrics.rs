//! Define all the metrics needed for chrony

use std::sync::atomic::AtomicU64;

use prometheus_client::{
    encoding::EncodeLabelSet,
    metrics::{counter::Counter, family::Family, gauge::Gauge, info::Info},
    registry::{Registry, Unit},
};
use tracing::trace;

#[derive(Default)]
pub(crate) struct ProcessMetrics {
    /// Total user and system CPU time in seconds
    pub cpu: Counter,
    /// Maximum number of open file descriptors
    pub max_fds: Gauge,
    /// Number of open file descriptors
    pub open_fds: Gauge,
    /// Resident memory size in bytes
    pub resident_memory: Gauge,
    /// Start time of the process since epoch in seconds
    pub start_time: Gauge,
    /// Number of OS threads in the process
    pub threads: Gauge,
    /// Virtual memory size in bytes
    pub virtual_memory: Gauge,
}

/// Prometheus metrics collected from chrony
#[derive(Default)]
pub(crate) struct ChronyMetrics {
    /// Estimated bias of the source in seconds
    pub source_offset: Family<Source, Gauge<f64, AtomicU64>>,
    /// Estimated standard deviation of the source in seconds
    pub source_std_dev: Family<Source, Gauge<f64, AtomicU64>>,
    /// How long ago the last good sample was received
    pub source_last_rx: Family<Source, Gauge>,
    /// Reachability register
    pub source_reach: Family<Source, Gauge>,
    /// Difference between system time and the "true" NTP time estimated by chrony in seconds
    pub system_offset: Gauge<f64, AtomicU64>,
    /// Estimated local offset on the last clock update in seconds
    pub last_offset: Gauge<f64, AtomicU64>,
    /// Long-term average of the last offset in seconds
    pub rms_offset: Gauge<f64, AtomicU64>,
    /// Sum of the network path delays to the stratum-1 server in seconds
    pub root_delay: Gauge<f64, AtomicU64>,
    /// Total dispersion accumualted to the stratum-1 server in seconds
    pub root_dispersion: Gauge<f64, AtomicU64>,
    /// The stratum of the chrony server
    pub stratum: Gauge,
}

pub(crate) fn init_metrics() -> (Registry, ProcessMetrics, ChronyMetrics) {
    let mut registry = Registry::default();
    let proc_metrics = ProcessMetrics::default();
    let metrics = ChronyMetrics::default();

    let info = vec![
        (
            "version",
            option_env!("VERGEN_GIT_DESCRIBE").unwrap_or(env!("CARGO_PKG_VERSION")),
        ),
        (
            "commit_date",
            option_env!("VERGEN_GIT_COMMIT_TIMESTAMP").unwrap_or("Unknown"),
        ),
        ("commit", option_env!("VERGEN_GIT_SHA").unwrap_or("Unknown")),
    ];

    registry.register(
        "chrony_exporter_build",
        "Build information for the chrony exporter",
        Info::new(info),
    );
    registry.register_with_unit(
        "chrony_source_offset",
        "Bias of the source in seconds",
        Unit::Seconds,
        metrics.source_offset.clone(),
    );
    registry.register_with_unit(
        "chrony_source_std_dev",
        "Standard deviation of the source in seconds",
        Unit::Seconds,
        metrics.source_std_dev.clone(),
    );
    registry.register_with_unit(
        "chrony_source_last_rx",
        "Age of the last good sample from the source in seconds",
        Unit::Seconds,
        metrics.source_last_rx.clone(),
    );
    registry.register(
        "chrony_source_reach",
        "Reachability register for the source",
        metrics.source_reach.clone(),
    );
    registry.register_with_unit(
        "chrony_system_offset",
        "Offset between system time and NTP time in seconds",
        Unit::Seconds,
        metrics.system_offset.clone(),
    );
    registry.register_with_unit(
        "chrony_last_offset",
        "Estimated local offset on the last clock update in seconds",
        Unit::Seconds,
        metrics.last_offset.clone(),
    );
    registry.register_with_unit(
        "chrony_rms_offset",
        "Long-term average of the last offset in seconds",
        Unit::Seconds,
        metrics.rms_offset.clone(),
    );
    registry.register_with_unit(
        "chrony_root_delay",
        "Total delay to the stratum-1 source in seconds",
        Unit::Seconds,
        metrics.root_delay.clone(),
    );
    registry.register_with_unit(
        "chrony_root_dispersion",
        "Total dispersion to the stratum-1 source in seconds",
        Unit::Seconds,
        metrics.root_dispersion.clone(),
    );
    registry.register(
        "chrony_stratum",
        "Number of hops away from the reference clock",
        metrics.stratum.clone(),
    );

    // Note that these should be using the proper units (bytes and seconds) but
    // I'm keeping compatible with the existing de-facto style
    registry.register(
        "process_cpu_seconds",
        "Total user and system CPU time spent in seconds",
        proc_metrics.cpu.clone(),
    );
    registry.register(
        "process_max_fds",
        "Maximum number of open file descriptors",
        proc_metrics.max_fds.clone(),
    );
    registry.register(
        "process_open_fds",
        "Number of open file descriptors",
        proc_metrics.open_fds.clone(),
    );
    registry.register(
        "process_resident_memory_bytes",
        "Resident memory size in bytes",
        proc_metrics.resident_memory.clone(),
    );
    registry.register(
        "process_start_time_seconds",
        "Start time of the process since unix epoch in seconds",
        proc_metrics.start_time.clone(),
    );
    registry.register(
        "process_threads",
        "Number of OS threads in the process",
        proc_metrics.threads.clone(),
    );
    registry.register(
        "process_virtual_memory_bytes",
        "Virtual memory size in bytes",
        proc_metrics.virtual_memory.clone(),
    );

    (registry, proc_metrics, metrics)
}

/// The label used for all metrics corresponding to sources.
#[derive(Clone, Debug, Hash, PartialEq, Eq, EncodeLabelSet)]
pub(crate) struct Source {
    pub(crate) name: String,
}

impl From<String> for Source {
    fn from(value: String) -> Self {
        Self { name: value }
    }
}

impl ProcessMetrics {
    /// Update the process metrics.
    ///
    /// This just queries the information from
    /// [`/proc`](https://www.man7.org/linux/man-pages/man5/proc.5.html) using
    /// the `procfs` crate.
    pub(crate) fn collect(&self) -> procfs::ProcResult<()> {
        use procfs::{
            boot_time_secs, process::LimitValue, process::Process, ticks_per_second,
            WithCurrentSystemInfo as _,
        };

        trace!("Querying /proc for process metrics");

        let p = Process::myself()?;

        let open_fds = p.fd_count()?;
        self.open_fds.set(open_fds.try_into().unwrap());

        let limits = p.limits()?;
        let max_fds = match limits.max_open_files.soft_limit {
            LimitValue::Value(v) => v.try_into().unwrap(),
            LimitValue::Unlimited => -1,
        };
        self.max_fds.set(max_fds);

        let stat = p.stat()?;
        self.threads.set(stat.num_threads);

        let user_ticks = stat.utime;
        let sys_ticks = stat.stime;
        let cpu_time = (user_ticks + sys_ticks) / ticks_per_second();
        // Note that the `cpu_time` here is essentially a gauge since it's the
        // accumulated total, but the `Counter` API doesn't permit setting the
        // exact value, only a non-negative increment.
        let prev_cpu = self.cpu.get();
        self.cpu.inc_by(cpu_time.saturating_sub(prev_cpu));

        let boot_time_since_epoch = boot_time_secs()?;
        let seconds_since_boot = stat.starttime / ticks_per_second();
        let time_since_epoch = seconds_since_boot + boot_time_since_epoch;
        self.start_time.set(time_since_epoch.try_into().unwrap());

        self.resident_memory
            .set(stat.rss_bytes().get().try_into().unwrap());
        self.virtual_memory.set(stat.vsize.try_into().unwrap());

        Ok(())
    }
}
