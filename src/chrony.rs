//! Read data from chrony and update the metrics

use super::metrics::ChronyMetrics;
use std::{io, str::FromStr, time::Duration};
use tracing::{debug, error, info, trace, warn};

/// How often to wait between chrony polls
const POLL_DELAY: Duration = Duration::from_secs(30);

/// Poll chrony periodically to update the statistics.
///
/// This is done by repeatedly launching `chronyc` in a subprocess and parsing
/// its output. Under normal conditions, it loops forever and is not expected to
/// return. The function may panic.
pub async fn poll_chronyc(metrics: ChronyMetrics) -> io::Result<()> {
    debug!("Connecting to chronyc");
    let cmd_version = tokio::process::Command::new("chronyc")
        .arg("-v")
        .output()
        .await
        .map_err(|e| {
            error!("couldn't spawn subprocess for chronyc: {}", e);
            e
        })?;
    info!(
        "chronyc version: {}",
        std::str::from_utf8(&cmd_version.stdout)
            .expect("cannot parse chronyc output as UTF-8")
            .trim()
    );

    // If we've successfully launched chronyc then we'll assume this function
    // can't error out any other way. So continually respawn chronyc, parse its
    // output, and update the statistics.
    loop {
        let sources = call_chronyc(&["-c", "sources"]).await;
        trace!("Parsing chronyc output to update metrics");
        for source in sources.lines().filter_map(|line| {
            line.parse::<ChronySources>()
                .map_err(|e| warn!("{}", e))
                .ok()
        }) {
            trace!(
                "source: {} selection state {:?}, reach {:#o}, and last-reception {} s ago",
                source.name,
                source.selection,
                source.reach,
                source.last_rx
            );
            source.update(&metrics);
        }

        let sourcestats = call_chronyc(&["-c", "sourcestats"]).await;
        trace!("Parsing chronyc output to update metrics");
        for sourcestat in sourcestats.lines().filter_map(|line| {
            line.parse::<ChronySourceStats>()
                .map_err(|e| warn!("{}", e))
                .ok()
        }) {
            trace!(
                "sourcestat: {} has offset {} and standard deviation {}",
                sourcestat.name,
                sourcestat.offset,
                sourcestat.standard_deviation
            );
            sourcestat.update(&metrics);
        }

        let tracking = call_chronyc(&["-c", "tracking"]).await;
        trace!("Parsing chronyc output to update metrics");
        match tracking.parse::<TrackingStats>() {
            Ok(tracking) => {
                trace!("tracking: {:?}", tracking);
                tracking.update(&metrics);
            }
            Err(e) => warn!("{}", e),
        }

        tokio::time::sleep(POLL_DELAY).await;
    }
}

/// Wrapper function to call `chronyc` with some arguments and check for errors
///
/// This will repeatedly loop until successful.
async fn call_chronyc(args: &[&str]) -> String {
    loop {
        trace!("Spawning chronyc {:?}", args);
        match tokio::process::Command::new("chronyc")
            .args(args)
            .output()
            .await
        {
            Ok(output) => {
                if output.status.success() {
                    return std::str::from_utf8(&output.stdout)
                        .expect("couldn't parse chronyc stdout to UTF-8")
                        .to_owned();
                } else {
                    warn!(
                        "chronyc command failed with code {}; stdout: '{:?}'; stderr: '{:?}'",
                        output.status.code().unwrap(),
                        std::str::from_utf8(&output.stdout),
                        std::str::from_utf8(&output.stderr),
                    );
                    tokio::time::sleep(POLL_DELAY).await;
                    continue;
                }
            }
            Err(e) => {
                warn!("Couldn't spawn subprocess for chronyc: {}", e);
                tokio::time::sleep(POLL_DELAY).await;
                continue;
            }
        };
    }
}

/// The parsed output from the `sources` chronyc command
#[derive(Debug)]
struct ChronySources {
    name: String,
    selection: SelectionState,
    last_rx: i64,
    reach: i64,
}

impl FromStr for ChronySources {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // The selection status is the second field of the CSV line, then the
        // name, the reach is the 6th field, and the last-reception time is the
        // 7th field
        let mut fields = s.split(',');
        let selection = fields
            .nth(1)
            .ok_or("couldn't find selection state")?
            .try_into()
            .or(Err("couldn't parse the selection state"))?;
        let name = fields.next().ok_or("couldn't find name")?.to_owned();
        // The reachability is an `i8` formatted in octal
        let reach = u8::from_str_radix(fields.nth(2).ok_or("couldn't find reach")?, 8)
            .or(Err("couldn't parse reach to an integer"))?
            .into();
        let last_rx = fields
            .next()
            .ok_or("couldn't find last-rx")?
            .parse()
            .or(Err("couldn't parse last-rx to an integer"))?;

        Ok(ChronySources {
            name,
            selection,
            last_rx,
            reach,
        })
    }
}

#[derive(Debug)]
enum SelectionState {
    SelectedAsBest,
    SelectedWithOthers,
    NotSelected,
    FalseTicker,
    TooVariable,
    Bad,
}

impl TryFrom<&str> for SelectionState {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "*" => Ok(SelectionState::SelectedAsBest),
            "+" => Ok(SelectionState::SelectedWithOthers),
            "-" => Ok(SelectionState::NotSelected),
            "x" => Ok(SelectionState::FalseTicker),
            "~" => Ok(SelectionState::TooVariable),
            "?" => Ok(SelectionState::Bad),
            _ => Err(()),
        }
    }
}

/// The parsed output from the `sourcestats` chronyc command
#[derive(Debug)]
struct ChronySourceStats {
    name: String,
    offset: f64,
    standard_deviation: f64,
}

impl FromStr for ChronySourceStats {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // The name is the first field of the CSV line, and the offset and
        // standard deviation are the 7th and 8th fields of the CSV line
        let mut fields = s.split(',');
        let name = fields.next().ok_or("couldn't find name")?.to_owned();
        let offset = fields
            .nth(5)
            .ok_or("couldn't find offset")?
            .parse()
            .or(Err("couldn't parse offset to a float"))?;
        let standard_deviation = fields
            .next()
            .ok_or("couldn't find std")?
            .parse()
            .or(Err("couldn't parse standard deviation to a float"))?;

        Ok(ChronySourceStats {
            name,
            offset,
            standard_deviation,
        })
    }
}

/// The parsed output from the `tracking` chronyc command
#[derive(Debug)]
struct TrackingStats {
    stratum: i64,
    system_offset: f64,
    last_offset: f64,
    rms_offset: f64,
    root_delay: f64,
    root_dispersion: f64,
}

impl FromStr for TrackingStats {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // The six fields of interest are entries 3, 5, 6, 7, 11, 12 of the CSV line
        let mut fields = s.split(',');
        let stratum = fields.nth(2);
        let system_offset = fields.nth(1);
        let last_offset = fields.next();
        let rms_offset = fields.next();
        let root_delay = fields.nth(3);
        let root_dispersion = fields.next();

        let stratum = stratum
            .ok_or("couldn't parse tracking line")
            .and_then(|o| {
                o.parse::<i64>()
                    .or(Err("couldn't parse stratum to integer"))
            })?;
        let system_offset = system_offset
            .ok_or("couldn't parse tracking line")
            .and_then(|o| {
                o.parse::<f64>()
                    .or(Err("couldn't parse system offset to float"))
            })?;
        let last_offset = last_offset
            .ok_or("couldn't parse tracking line")
            .and_then(|o| {
                o.parse::<f64>()
                    .or(Err("couldn't parse last offset to float"))
            })?;
        let rms_offset = rms_offset
            .ok_or("couldn't parse tracking line")
            .and_then(|o| {
                o.parse::<f64>()
                    .or(Err("couldn't parse last offset to float"))
            })?;
        let root_delay = root_delay
            .ok_or("couldn't parse tracking line")
            .and_then(|o| {
                o.parse::<f64>()
                    .or(Err("couldn't parse last offset to float"))
            })?;
        let root_dispersion = root_dispersion
            .ok_or("couldn't parse tracking line")
            .and_then(|o| {
                o.parse::<f64>()
                    .or(Err("couldn't parse last offset to float"))
            })?;

        Ok(TrackingStats {
            stratum,
            system_offset,
            last_offset,
            rms_offset,
            root_delay,
            root_dispersion,
        })
    }
}

impl ChronySources {
    /// Update the metrics.
    fn update(&self, metrics: &ChronyMetrics) {
        let label = self.name.clone().into();
        metrics
            .source_last_rx
            .get_or_create(&label)
            .set(self.last_rx);
        metrics.source_reach.get_or_create(&label).set(self.reach);
    }
}

impl ChronySourceStats {
    /// Update the metrics.
    fn update(&self, metrics: &ChronyMetrics) {
        let label = self.name.clone().into();
        metrics.source_offset.get_or_create(&label).set(self.offset);
        metrics
            .source_std_dev
            .get_or_create(&label)
            .set(self.standard_deviation);
    }
}

impl TrackingStats {
    /// Update the metrics.
    fn update(&self, metrics: &ChronyMetrics) {
        metrics.stratum.set(self.stratum);
        metrics.system_offset.set(self.system_offset);
        metrics.last_offset.set(self.last_offset);
        metrics.rms_offset.set(self.rms_offset);
        metrics.root_delay.set(self.root_delay);
        metrics.root_dispersion.set(self.root_dispersion);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_tracking() {
        let example_line = "50505300,PPS,1,1638648906.573139201,-0.000620169,-0.000000167,0.000105435,0.104,-0.000,0.007,0.000000001,0.000011188,16.0,Normal";
        let parsed = example_line.parse::<TrackingStats>().unwrap();

        assert_eq!(parsed.stratum, 1);
        assert_eq!(parsed.system_offset, -0.000620169);
        assert_eq!(parsed.last_offset, -0.000000167);
        assert_eq!(parsed.rms_offset, 0.000105435);
        assert_eq!(parsed.root_delay, 0.000000001);
        assert_eq!(parsed.root_dispersion, 0.000011188);
    }

    #[test]
    fn parse_sourcestats() {
        let example_lines = concat!(
            "GPS,7,6,95,-11.556,23.774,0.138197973,0.000266480\n",
            "PPS,60,31,943,-0.000,0.001,-0.000000000,0.000000356\n",
            "194.0.5.123,17,11,17578,0.045,0.049,-0.000667101,0.000205784\n",
            "64.142.54.12,9,6,4142,0.154,0.092,-0.002716230,0.000070216\n",
            "199.101.96.52,12,6,4388,0.410,0.138,-0.002951743,0.000137714\n",
            "192.48.105.15,6,3,2581,0.063,0.496,-0.000770624,0.000107718\n"
        );

        let parsed = example_lines
            .lines()
            .map(|line| line.parse::<ChronySourceStats>().unwrap())
            .find(|stat| stat.name == "PPS")
            .unwrap();
        assert_eq!(parsed.offset, -0.000000000);
        assert_eq!(parsed.standard_deviation, 0.000000356);
    }

    #[test]
    fn parse_sources() {
        let example_lines = concat!(
            "#,?,GPS,0,4,77,13,0.521156132,0.521156132,0.001212245\n",
            "#,*,PPS,0,4,237,19,-0.000000566,-0.000000622,0.000000162\n",
            "^,-,194.0.5.123,2,10,377,97,0.000148712,0.000148653,0.026778726\n",
            "^,-,64.142.54.12,3,8,377,195,-0.002366848,-0.002366865,0.120089754\n",
            "^,-,199.101.96.52,2,9,377,158,-0.002994227,-0.002994268,0.062665105\n",
            "^,-,192.48.105.15,2,9,377,273,-0.001660969,-0.001660998,0.083012328\n",
        );

        let parsed = example_lines
            .lines()
            .map(|line| line.parse::<ChronySources>().unwrap())
            .find(|stat| stat.name == "PPS")
            .unwrap();
        assert!(matches!(parsed.selection, SelectionState::SelectedAsBest));
        assert_eq!(parsed.last_rx, 19);
        assert_eq!(parsed.reach, 0o237);
    }
}
