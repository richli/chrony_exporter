//! Wrappers for the HTTP serving

use axum::{extract::Request, extract::State, middleware::Next, response::Response};
use http::{header, HeaderMap, HeaderValue, StatusCode};
use libsystemd::activation::{self, IsType};
use prometheus_client::{encoding, registry::Registry};
use std::{
    error::Error,
    fmt, net,
    os::fd::{FromRawFd, IntoRawFd, OwnedFd},
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
};
use tracing::{debug, info, trace, warn};

use crate::metrics::ProcessMetrics;

/// Possible ways the HTTP listener will fail.
#[derive(Debug)]
pub enum ListenerError {
    /// Some issue with the systemd-provided socket.
    Systemd(String),
    /// An io error.
    Io(std::io::Error),
}

impl fmt::Display for ListenerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            ListenerError::Systemd(msg) => {
                write!(f, "error from systemd socket-activation: {msg}")
            }
            ListenerError::Io(msg) => write!(f, "io error: {msg}"),
        }
    }
}

impl From<std::io::Error> for ListenerError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl Error for ListenerError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            ListenerError::Systemd(_) => None,
            ListenerError::Io(e) => Some(e),
        }
    }
}

/// Create a valid [`std::net::TcpListener`].
///
/// If systemd passes in a socket, then use it. Otherwise, bind one based on the
/// inputs.
pub fn prepare_listener(
    listen_addresses: &[net::SocketAddr],
) -> Result<net::TcpListener, ListenerError> {
    // Check if systemd passed in a socket; if it didn't then try using the input addresses
    let listener = activation::receive_descriptors(false).map_or_else(
        |_| {
            debug!("Attempting to bind on {:?}", &listen_addresses);
            net::TcpListener::bind(listen_addresses).map_err(ListenerError::from)
        },
        |sd_fds| {
            // We only expect exactly one socket
            if sd_fds.len() != 1 {
                return Err(ListenerError::Systemd(format!(
                    "expected only one listening socket from systemd, but received: {}",
                    sd_fds.len()
                )));
            }

            // This unwrap is safe since the length is checked above
            let fd = sd_fds.into_iter().next().unwrap();

            // Check that the file descriptor is as expected. It must be a TCP
            // stream listener, so this checks both AF_INET and AF_INET6. (Without
            // the IP family checks, it could still be a stream listener, but a Unix
            // domain socket. I.e, we want to make sure we have a
            // std::net::TcpListener and not necessarily a
            // std::os::unix::net::UnixListener.)
            if !fd.is_inet() {
                return Err(ListenerError::Systemd(
                    "file descriptor passed in by systemd is not a TCP listener".into(),
                ));
            }

            // SAFETY: according to the `sd_listen_fds(3)` call, ownership is
            // transferred here
            let fd = unsafe { OwnedFd::from_raw_fd(fd.into_raw_fd()) };

            // The above checks ensure that the file descriptor really is a
            // TCP listener
            debug!("Accepting listener socket passed in by systemd");
            Ok(net::TcpListener::from(fd))
        },
    )?;

    info!(
        "Listening for connections on {}",
        listener.local_addr().unwrap()
    );
    Ok(listener)
}

/// Return the current metrics.
///
/// This is intended to be used for a `GET` request on `/metrics`.
pub(crate) async fn get_metrics(
    State(state): State<Arc<ServerState>>,
) -> (StatusCode, HeaderMap, String) {
    state.update_process_metrics();
    let mut output = String::new();
    encoding::text::encode(&mut output, state.get_registry()).expect("cannot export metrics");

    let mut headers = HeaderMap::new();
    headers.insert(
        header::CONTENT_TYPE,
        HeaderValue::from_static("application/openmetrics-text; version=1.0.0; charset=utf-8"),
    );
    headers.insert(
        header::CACHE_CONTROL,
        HeaderValue::from_static("max-age=10"),
    );
    (StatusCode::OK, headers, output)
}

/// Redirect to `/metrics`
pub(crate) async fn redirect_metrics() -> (StatusCode, HeaderMap, String) {
    let mut headers = HeaderMap::new();
    headers.insert(header::LOCATION, HeaderValue::from_static("/metrics"));
    (
        StatusCode::TEMPORARY_REDIRECT,
        headers,
        "See /metrics\n".to_owned(),
    )
}

/// Middleware to increment the request counter
pub(crate) async fn count_requests(
    State(state): State<Arc<ServerState>>,
    request: Request,
    next: Next,
) -> Response {
    let num_requests = state.increment_request();
    trace!("Request counter: {}", num_requests);

    next.run(request).await
}

pub struct ServerState {
    /// Number of HTTP requests handled since the server started
    requests: Arc<AtomicUsize>,
    /// Registry of prometheus metrics
    registry: Registry,
    /// Process metrics
    proc_metrics: ProcessMetrics,
}

impl ServerState {
    pub fn new(registry: Registry, proc_metrics: ProcessMetrics) -> Self {
        Self {
            requests: Default::default(),
            registry,
            proc_metrics,
        }
    }

    pub fn get_registry(&self) -> &Registry {
        &self.registry
    }

    /// Update the process-related metrics
    pub fn update_process_metrics(&self) {
        if let Err(e) = self.proc_metrics.collect() {
            warn!("{e}");
        }
    }

    /// Return the number of requests serviced
    pub fn num_requests(&self) -> usize {
        self.requests.load(Ordering::Relaxed)
    }

    /// Increment the number of requests, returning the previous number serviced
    pub fn increment_request(&self) -> usize {
        self.requests.fetch_add(1, Ordering::Relaxed)
    }
}
